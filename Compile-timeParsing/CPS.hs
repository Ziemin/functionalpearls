{-# LANGUAGE RankNTypes
           , TypeSynonymInstances
 #-}

module CPS where

type CPS a = forall r. (a -> r) -> r

-- | Code below is necessary when CPS is a newtype.
-- | Since >>= is basically function application with no effects, we will just apply
-- functions

-- instance Monad CPS where
--     return a = ($ a)
--     m >>= f = m f
--
-- instance Applicative CPS where
--     pure = return
--     mf <*> ma = do
--       f <- mf
--       a <- ma
--       return (f a)
--
-- instance Functor CPS where
--     fmap f m = do
--       a <- m
--       return (f a)

return' :: a -> CPS a
return' a = ($ a)

lift :: (a -> b) -> a -> CPS b
lift f a = return' (f a)

run :: CPS a -> a
run m = m id
