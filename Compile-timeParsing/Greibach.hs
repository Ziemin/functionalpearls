{-# LANGUAGE RankNTypes
           , FunctionalDependencies
           , FlexibleInstances
 #-}

module Greibach where

import CPS

type Var = String

-- | AST for grammar:
-- S -> id C E
--    | if E S S
--    | while E S
--    | begin S R
-- C -> :=
-- E -> id
-- R -> end | ; S R
data Stat = Set Var Var
          | If Var Stat Stat
          | While Var Stat
          | Begin [Stat]
          deriving (Show)

-- | nonterminals representation
newtype S r = S (Stat   -> r)
newtype C r = C            r
newtype E r = E (Var    -> r)
newtype R r = R ([Stat] -> r)

-- | State is a stack of nonterminal symbols. Active terminal selects
-- a production by looking at topmost symbol on the stack. If grammar is
-- unabiguous then there is at most one suitable production.
-- The nonterminal is then replaced by the right-hand side of the
-- production (omitting the leading terminal)

-- | Using these type-level nonterminals we can program the type checker to
-- parse quotations: each production A -> aB1 .. Bn is mapped to a function
-- a, the active terminal of type A r -> CPS (B1 (... (Bn r)) ... ) that
-- implements the expansion of A.
--
-- The terminal a may appear in in different productions, so it cannot
-- possibly translate to a single function. Rather it stands for a family
-- of functions represented by type classes.


class Id lhs rhs | lhs -> rhs where  -- functinal depencency here avoids ambiguities
                                     -- during type-inference making use of
                                     -- the fact that grammar in GNF is
                                     -- unambiguous
    id' :: String -> (lhs -> CPS rhs)

instance Id (S r) (C (E r)) where
    id' l = lift $ \(S ctx) -> C (E (\r -> ctx (Set l r)))

if'     = lift $ \(S ctx) -> E (\c -> S (\t -> S (\e -> ctx (If c t e))))

while'  = lift $ \(S ctx) -> E (\c -> S (\t -> ctx (While c t)))

begin'  = lift $ \(S ctx) -> S (\s -> R (\r -> ctx (Begin $ s:r)))

set'    = lift $ \(C ctx) -> ctx

instance Id (E r) r where
    id' l = lift $ \(E ctx) -> ctx l

end'    = lift $ \(R ctx) -> ctx []

sep'    = lift $ \(R ctx) -> S (\s -> R (\r -> ctx $ s:r))

quote   = return' $ S (\s -> s)

endquote = id

x, y, z :: (Id lhs rhs) => lhs -> CPS rhs
x = id' "x"
y = id' "y"
z = id' "z"


exCode1 :: Stat
exCode1 = quote while' x y set' z endquote
-- show exCode1 == While "x" (Set "y" "z")

exCode2 :: Stat
exCode2 = quote begin'
                x set' y sep'
                if' x
                   y set' z
                   z set' y
                end' endquote
-- show exCode2 == Begin [ Set "x" "y", If "x" (Set "y" "z") (Set "z" "y")]

-- | invalid examples

-- exCode4 = quote while' if' x z endquote
-- exCode5 :: Stat
-- exCode5 = quote if' x if' x x set' z x set' y endquote
