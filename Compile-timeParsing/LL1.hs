{-# LANGUAGE RankNTypes
           , FunctionalDependencies
           , FlexibleInstances
           , UndecidableInstances
 #-}

module LL1 where

import Prelude hiding ((+), (*), (<), (>), (>>), (<<))
import CPS

-- | Arithmetic expressions grammar
-- E -> E + T                E  -> T E'
-- T -> T * F | F       =>   E' -> + T E' | e
-- F -> ( E ) | id           T  -> F T'
--                           T' -> * F T' | e
--                           F  -> ( E ) | id

data Expr = Id String
          | Add Expr Expr
          | Mul Expr Expr
          deriving (Show)

-- | Parsing grammar table
-- |    ||     id    |      +       |      *       | (          |     )   |   >>    |
-- |====||===========|==============|==============|============|=========|=========|
-- | E  || E -> T E' |              |              | E -> T E'  |         |         |
-- | E' ||           | E' -> + T E' |              |            | E' -> e | E' -> e |
-- | T  || T -> F T' |              |              | T -> F T'  |         |         |
-- | T' ||           |   T' -> e    | T' -> * F T' |            | T' -> e | T' -> e |
-- | F  || F -> id   |              |              | F -> ( E ) |         |         |

-- | The state is now stack of pending symbols (terminals, nontermials).
-- Symbol X on top of the stack determines the action of the current active
-- terminal a.
-- If X = a then the terminal pops X from the stack and passes
-- control to the next active terminal (pop action).
-- If X is nonterminal, then the terminal looks up the production indexed by
-- X and a in the parsing table, replaces X by the right-hand side of the
-- production, and remains active (expand action).

-- | Since the state now is the stack of symbols, therefore we must introduce types
-- both for terminal and nonterminal symbols.
-- Terminals carry semantic information:
-- The terminal id s, for instance, returns the semantic value Id s of type
-- Expr
newtype I r = I (Expr           -> r) -- id
newtype A r = A                    r  -- +
newtype M r = M                    r  -- *
newtype O r = O                    r  -- (
newtype C r = C                    r  -- )

-- | E' and T' yield expressions with a hole, where the hole stands for the
-- | missing left argument of the operator
newtype E  r = E  (Expr           -> r)
newtype E' r = E' ((Expr -> Expr) -> r)
newtype T  r = T  (Expr           -> r)
newtype T' r = T' ((Expr -> Expr) -> r)
newtype F  r = F  (Expr           -> r)

class Id    old new | old -> new where id'     :: String -> old -> CPS new
class Add   old new | old -> new where (+)     ::           old -> CPS new
class Mul   old new | old -> new where (*)     ::           old -> CPS new
class Open  old new | old -> new where (<)     ::           old -> CPS new
class Close old new | old -> new where (>)     ::           old -> CPS new
class Endquote old               where (>>)    ::           old -> Expr


-- | The instance head always reflects the parsing state after the first
-- pop

instance Id (E r) (T' (E' r)) where                         -- expand: E -> T E'
    id' s (E ctx) = id' s (T (\t -> E' (\e' -> ctx (e' t))))

instance Id (T r) (T' r) where                              -- expand: T -> F T'
    id' s (T ctx) = id' s (F (\f -> T' (\t' -> ctx (t' f))))

instance Id (F r) r where                                   -- expand: F -> id
    id' s (F ctx) = id' s (I (\v -> ctx v))

instance Id (I r) r where                                   -- pop
    id' s (I ctx) = return' (ctx (Id s))

instance Add (E' r) (T (E' r)) where                        -- expand: E' -> + T E'
    (+) (E' ctx) = (+) (A (T (\t -> E' (\e' -> ctx (\l -> e' (Add l t))))))

instance (Add r r') => Add (T' r) r' where                  -- expand: T' -> e
    (+) (T' ctx) = (+) (ctx (\e -> e))

instance Add (A r) r where                                  -- pop
    (+) (A ctx) = return' ctx

instance Mul (T' r) (F (T' r)) where                        -- expand: T' -> * F T'
    (*) (T' ctx) = (*) (M (F (\f -> T' (\t' -> ctx (\l -> t' (Mul l f))))))

instance Mul (M r) r where                                  -- pop
    (*) (M ctx) = return' ctx

instance Open (E r) (E (C (T' (E' r)))) where               -- expand: E -> T E'
    (<) (E ctx) = (<) (T (\t -> E' (\e' -> ctx (e' t))))

instance Open (T r) (E (C (T' r))) where                    -- expand: T -> F T'
    (<) (T ctx) = (<) ( F (\f -> T' (\t' -> ctx (t' f))))

instance Open (F r) (E (C r)) where                         -- expand: F -> ( E )
    (<) (F ctx) = (<) (O (E (\e -> C (ctx e))))

instance Open (O r) r where                                 -- pop
    (<) (O ctx) = return' ctx

instance (Close r r') => Close (E' r) r' where              -- expand: E' -> e
    (>) (E' ctx) = (>) (ctx (\e -> e))

instance (Close r r') => Close (T' r) r' where              -- expand: T' -> e
    (>) (T' ctx) = (>) (ctx (\e -> e))

instance Close (C r) r where                                -- pop
    (>) (C ctx) = return' ctx

instance (Endquote r) => Endquote (E' r) where              -- expand: E' -> e
    (>>) (E' ctx) = (>>) (ctx (\e -> e))

instance (Endquote r) => Endquote (T' r) where              -- expand: T' -> e
    (>>) (T' ctx) = (>>) (ctx (\e -> e))

instance Endquote Expr where                                -- pop
    (>>) e = e

(<<) = return' (E (\e -> e))

antiquote :: E s -> Expr -> CPS s
antiquote (E st) e = return' (st e)
a' = antiquote

x, y, z :: (Id old new) => old -> CPS new
x = id' "x"
y = id' "y"
z = id' "z"

ex1 :: Expr
ex1 = (<<) x (+) y (>>)
-- show ex1 == Add (Id "x") (Id "y")

ex2 :: Expr
ex2 = (<<) x (+) y (+) z (>>)
-- show ex2 == Add (Add (Id "x") (Id "y")) (Id "z")

ex3 :: Expr
ex3 = (<<) x (+) (<) a' (foldr1 Add [Id (show i) | i <- [1..3]]) (>) (+) y (>>)
-- show ex3 == Add (Add (Id "x") (Add (Id "1") (Add (Id "2") (Id "3")))) (Id "y")

ex4 :: Expr
ex4 = (<<) x (+) y (*) z (>>)
-- show ex4 == Add (Id "x") (Mul (Id "y") (Id "z"))

ex5 :: Expr
ex5 = (<<) x (*) (<) x (*) z (+) y (>) (>>)
-- show ex5 == Mul (Id "x") (Add (Mul (Id "x") (Id "z")) (Id "y"))

ex6 :: Expr
ex6 = (<<) x (*) (<) (<) x (+) z (>) (*) z (>) (>>)
-- show ex6 == Mul (Id "x") (Mul (Add (Id "x") (Id "z")) (Id "z"))
