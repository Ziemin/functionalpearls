{-# LANGUAGE RankNTypes
           , FunctionalDependencies
           , FlexibleInstances
           , UndecidableInstances
           , FlexibleContexts
 #-}


module LR0 where

import Prelude hiding ((<), (>), (>>), (<<))
import Types
import CPS

-- | The language of balanced parentheses
--        P -> e | P ( P )
-- AST is represented by Tree

-- LR(0) finite state automaton
--
--    S -> . P >>           P        S -> P . >>        >>
-- 1: P -> .             ------>  2: P -> P . ( P )  -------> 3: S -> P >> .
--    P -> . P ( P )
--                               /
--                              /
--                           ( /
--                            /
--                         /_/
--    P -> P ( . P )       \                             )
-- 5: P -> .          ---- P ---> 4: P -> P ( P . )  --------> 6: P -> P ( P ) .
--    P -> . P ( P )                 P -> P . ( P )
--                                   |
--                  \-------- ( -------
--

-- 2, 4 are shift states - consuming input tokens and pushing them
-- on to the stack
--
-- 1, 3, 5, 6 are reduce states. In reduce state the right hand side of the
-- production resides on top of the stack, which is then replaced by the
-- left hand side of the production on the stack
--
-- There are to sequences to get to state 6:
-- 1 -P-> 2 -(-> 5 -P-> 4 -)-> 6
-- 5 -P-> 4 -(-> 5 -P-> 4 -)-> 6
-- Removing P (P) from the stack means returning either to 1 or 5
-- Pushing P we move to either state 2 or 4. In short, reducing P -> P (P)
-- is accomplished by replacing the above transitions by either 1 -P-> 2 or 5 -P-> 4


-- | The parser state is a stack of LR(0) states

data S1    = S1           -- S
data S2 st = S2 Tree st   -- P
data S3 st = S3      st   -- >>
data S4 st = S4 Tree st   -- P
data S5 st = S5      st   -- (
data S6 st = S6      st   -- )

-- states are translated to functions that performs the corresponding
-- actions
--
-- shift states delegates control to the next active terminal
-- reduce states pops the transitions corresponding to the right-hand side
-- from the stack and pushes a transitions corresponding to the left-hand
-- side.
--

(<<)                  = state1 S1                                    -- start

state1 st             = state2 (S2 Leaf st)                          -- reduce

state2 st             = return' st                                   -- shift

state3 (S3 (S2 t S1)) = t                                            -- accept

state4 st             = return' st                                   -- shift

state5 st             = state4 (S4 Leaf st)                          -- reduce

class State6 old new | old -> new where
    state6          :: old -> CPS new

instance State6 (S6 (S4 (S5 (S2 S1)))) (S2 S1) where                 -- reduce
    state6 (S6 (S4 u (S5 (S2 t S1)))) = state2 (S2 (Fork t u ) S1)

instance State6 (S6 (S4 (S5 (S4 (S5 s))))) (S4 (S5 s)) where       -- reduce
    state6 (S6 (S4 u (S5 (S4 t (S5 s))))) = state4 (S4 (Fork t u) (S5 s))

-- | The active terminals implement the shift actions

class Open old new | old -> new where
    left          :: old -> CPS new

-- | Instance types reflect stack modifications to the next shift
-- e. g. ( moves from S2 to S5 and the to S4, which is again a shift state
instance Open (S2 s) (S4 (S5 (S2 s))) where
    left st@(S2 _ _)  = state5 (S5 st)

instance Open (S4 s) (S4 (S5 (S4 s))) where
    left st@(S4 _ _)  = state5 (S5 st)

right st@(S4 _ _)     = state6 (S6 st)

(>>) st@(S2 _ _)      = state3 (S3 st)


ex1 :: Tree
ex1 = (<<) left right (>>)
-- show ex1 == Fork Leaf Leaf

ex2 :: Tree
ex2 = (<<) (>>)
-- show ex2 == Leaf

ex3 :: Tree
ex3 = (<<) left left left right left right right left right right (>>)
-- show ex3 == Fork Leaf (Fork (Fork Leaf (Fork (Fork Leaf Leaf) Leaf)) Leaf)

-- missing closing parenthesis
{-ex4 :: Tree-}
{-ex4 = (<<) left left right (>>)-}
