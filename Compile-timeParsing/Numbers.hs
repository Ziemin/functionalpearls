module Numbers where

import CPS

-- Simple continuation passing style tick numbers
quote f = f 0
tick n f = f (succ n)
endquote n = n

four = quote tick tick tick tick endquote

-- Monadic tick numbers
quoteM :: CPS Int
quoteM = return 0

tickM, endquoteM :: Int -> CPS Int
tickM = lift succ
endquoteM = lift id

fourM = run (quoteM >>= tickM >>= tickM >>= tickM >>= tickM)
