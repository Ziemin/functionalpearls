{-# LANGUAGE RankNTypes #-}

module Postfix where

import Types
import CPS

-- | Function follows its arguments.
-- | It finds them on stack then it is applied to them, they are popped and
-- | the result of the application is pushed on the stack

leafF :: s -> (s, Tree)
leafF st = (st, Leaf)

forkF :: ((s, Tree), Tree) -> (s, Tree)
forkF ((s, l), r) = (s, Fork l r)

leaf :: s -> CPS (s, Tree)
leaf = lift leafF

fork :: ((s, Tree), Tree) -> CPS (s, Tree)
fork = lift forkF

quote :: CPS ()
quote = return' ()

endquote :: ((), Tree) -> Tree
endquote ((), t) = t

antiquote :: s -> Tree -> CPS (s, Tree)
antiquote st t = return' (st, t)

e1 = quote                                    :: CPS ()
e2 = quote leaf                               :: CPS ((), Tree)
e3 = quote leaf leaf                          :: CPS (((), Tree), Tree)
e4 = quote leaf leaf fork                     :: CPS ((), Tree)
e5 = quote leaf leaf fork leaf                :: CPS (((), Tree), Tree)
e6 = quote leaf leaf fork leaf fork           :: CPS ((), Tree)
e7 = quote leaf leaf fork leaf fork endquote  :: Tree
-- show e7 == Fork (Fork Leaf Leaf) Leaf

-- | invalid examples

-- e8 = quote tree
-- e9 = quote leaf leaf endquote
