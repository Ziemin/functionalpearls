{-# LANGUAGE RankNTypes #-}

module Prefix where

import Types
import CPS

-- | In prefix notation function precedes its arguments.
-- | The state is a stack of pending arguments.
-- | First arguments of leafF forkF (data constructors in a sense) can be
-- | seen as a request for value. The prefix constructor will sattisfy this
-- | request, but in turn generates request for its arguments.

leafF :: ((Tree, s) -> r) -> (s -> r)
leafF ctx = \st -> ctx (Leaf, st)

forkF :: ((Tree, s) -> r) -> ((Tree, (Tree, s)) -> r)
forkF ctx = \(t, (u, st)) -> ctx (Fork t u, st)

quote :: CPS ((Tree, ()) -> Tree)
quote = return' (\(t, ()) -> t)

leaf :: ((Tree, s) -> r) -> CPS (s -> r)
leaf = lift leafF

fork :: ((Tree, s) -> r) -> CPS ((Tree, (Tree, s)) -> r)
fork = lift forkF

endquote :: (() -> Tree) -> Tree
endquote ctx = ctx ()

antiquote ctx t = return' (\st -> ctx (t, st))


e1 = quote                                    :: CPS ((Tree, ()) -> Tree)
e2 = quote fork                               :: CPS ((Tree, (Tree, ())) -> Tree)
e3 = quote fork fork                          :: CPS ((Tree, (Tree, (Tree, ()))) -> Tree)
e4 = quote fork fork leaf                     :: CPS ((Tree, (Tree, ())) -> Tree)
e5 = quote fork fork leaf leaf                :: CPS ((Tree, ()) -> Tree)
e6 = quote fork fork leaf leaf leaf           :: CPS (() -> Tree)
e7 = quote fork fork leaf leaf leaf endquote  :: Tree

-- dhoe e7 == Fork (Fork Leaf Leaf) Leaf

-- | invalid examples

-- e8 = quote fork endquote
-- e9 = quote leaf leaf endquote
