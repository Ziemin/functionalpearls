module Types where

data Tree = Leaf | Fork Tree Tree deriving (Show)
